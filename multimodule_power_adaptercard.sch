EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Multimodule Adapter Card"
Date "2020-12-23"
Rev "1.0"
Comp "LBNL (IJCLAB)"
Comment1 "Aleksandra Dimitrievska (modified by Dmytro)"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x03 NTC1
U 1 1 600D4842
P 10850 2800
F 0 "NTC1" H 10930 2842 50  0000 L CNN
F 1 "Conn_01x03" H 10930 2751 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10850 2800 50  0001 C CNN
F 3 "~" H 10850 2800 50  0001 C CNN
	1    10850 2800
	1    0    0    -1  
$EndComp
Text Label 10650 2700 2    50   ~ 0
NTC1
$Comp
L Device:R R1
U 1 1 600D9B0F
P 9850 4750
F 0 "R1" V 9643 4750 50  0000 C CNN
F 1 "50k" V 9734 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 9780 4750 50  0001 C CNN
F 3 "~" H 9850 4750 50  0001 C CNN
	1    9850 4750
	0    1    1    0   
$EndComp
Text Label 10650 2800 2    50   ~ 0
NTC_RET1
Wire Wire Line
	10500 4750 10000 4750
Wire Wire Line
	10500 4650 10000 4650
Wire Wire Line
	10500 4950 10000 4950
Wire Wire Line
	10500 5050 10000 5050
Wire Wire Line
	10500 5150 10000 5150
Wire Wire Line
	12000 4950 11500 4950
Wire Wire Line
	12000 5050 11500 5050
Wire Wire Line
	12000 5150 11500 5150
Text Label 12000 4950 2    50   ~ 0
GND
Text Label 12000 5050 2    50   ~ 0
GND
Text Label 12000 5150 2    50   ~ 0
GND
Text Label 10000 5150 0    50   ~ 0
GND
Text Label 10000 5050 0    50   ~ 0
GND
Text Label 10000 4950 0    50   ~ 0
GND
Text Label 10000 4650 0    50   ~ 0
NTC1
Text Label 10000 4750 0    50   ~ 0
NTC_RET1
$Comp
L multimodule_power_adaptercard-rescue:FTM-108-02-L-DV-FTM-108-02-L-DV Module2
U 1 1 6488DE0D
P 11100 6200
F 0 "Module2" H 11100 6767 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 11100 6676 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 11100 6200 50  0001 L BNN
F 3 "E" H 11100 6200 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 11100 6200 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 11100 6200 50  0001 L BNN "Field5"
	1    11100 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 5900 10100 5900
Wire Wire Line
	10600 6000 10100 6000
Wire Wire Line
	10600 6200 10100 6200
Wire Wire Line
	10600 6100 10100 6100
Wire Wire Line
	10600 6400 10100 6400
Wire Wire Line
	10600 6500 10100 6500
Wire Wire Line
	10600 6600 10100 6600
Wire Wire Line
	12100 6200 11600 6200
Wire Wire Line
	12100 6400 11600 6400
Wire Wire Line
	12100 6500 11600 6500
Wire Wire Line
	12100 6600 11600 6600
Text Label 12100 6400 2    50   ~ 0
GND
Text Label 12100 6500 2    50   ~ 0
GND
Text Label 12100 6600 2    50   ~ 0
GND
Text Label 10100 6600 0    50   ~ 0
GND
Text Label 10100 6500 0    50   ~ 0
GND
Text Label 10100 6400 0    50   ~ 0
GND
Text Label 10100 6100 0    50   ~ 0
NTC2
Text Label 10100 6200 0    50   ~ 0
NTC_RET2
Wire Wire Line
	12100 6100 11600 6100
Wire Wire Line
	12100 6000 11600 6000
$Comp
L multimodule_power_adaptercard-rescue:FTM-108-02-L-DV-FTM-108-02-L-DV Module6
U 1 1 64890842
P 14800 6200
F 0 "Module6" H 14800 6767 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 14800 6676 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 14800 6200 50  0001 L BNN
F 3 "E" H 14800 6200 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 14800 6200 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 14800 6200 50  0001 L BNN "Field5"
	1    14800 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	14300 6200 13800 6200
Wire Wire Line
	14300 6100 13800 6100
Wire Wire Line
	14300 6400 13800 6400
Wire Wire Line
	14300 6500 13800 6500
Wire Wire Line
	14300 6600 13800 6600
Wire Wire Line
	15800 6400 15300 6400
Wire Wire Line
	15800 6500 15300 6500
Wire Wire Line
	15800 6600 15300 6600
Text Label 15800 6400 2    50   ~ 0
GND
Text Label 15800 6500 2    50   ~ 0
GND
Text Label 15800 6600 2    50   ~ 0
GND
Text Label 13800 6600 0    50   ~ 0
GND
Text Label 13800 6500 0    50   ~ 0
GND
Text Label 13800 6400 0    50   ~ 0
GND
Text Label 13800 6100 0    50   ~ 0
NTC6
Text Label 13800 6200 0    50   ~ 0
NTC_RET6
$Comp
L multimodule_power_adaptercard-rescue:FTM-108-02-L-DV-FTM-108-02-L-DV Module3
U 1 1 6489447D
P 11200 7650
F 0 "Module3" H 11200 8217 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 11200 8126 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 11200 7650 50  0001 L BNN
F 3 "E" H 11200 7650 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 11200 7650 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 11200 7650 50  0001 L BNN "Field5"
	1    11200 7650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 7350 10200 7350
Wire Wire Line
	10700 7450 10200 7450
Wire Wire Line
	10700 7650 10200 7650
Wire Wire Line
	10700 7550 10200 7550
Wire Wire Line
	10700 7850 10200 7850
Wire Wire Line
	10700 7950 10200 7950
Wire Wire Line
	10700 8050 10200 8050
Wire Wire Line
	12200 7650 11700 7650
Wire Wire Line
	12200 7850 11700 7850
Wire Wire Line
	12200 7950 11700 7950
Wire Wire Line
	12200 8050 11700 8050
Text Label 10200 7350 0    50   ~ 0
VIN_3
Text Label 12200 7850 2    50   ~ 0
GND
Text Label 12200 7950 2    50   ~ 0
GND
Text Label 12200 8050 2    50   ~ 0
GND
Text Label 10200 8050 0    50   ~ 0
GND
Text Label 10200 7950 0    50   ~ 0
GND
Text Label 10200 7850 0    50   ~ 0
GND
Text Label 10200 7550 0    50   ~ 0
NTC3
Text Label 10200 7650 0    50   ~ 0
NTC_RET3
Wire Wire Line
	12200 7550 11700 7550
Wire Wire Line
	12200 7450 11700 7450
$Comp
L multimodule_power_adaptercard-rescue:FTM-108-02-L-DV-FTM-108-02-L-DV Module7
U 1 1 648991DB
P 14750 7750
F 0 "Module7" H 14750 8317 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 14750 8226 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 14750 7750 50  0001 L BNN
F 3 "E" H 14750 7750 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 14750 7750 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 14750 7750 50  0001 L BNN "Field5"
	1    14750 7750
	1    0    0    -1  
$EndComp
Wire Wire Line
	14250 7450 13750 7450
Wire Wire Line
	14250 7550 13750 7550
Wire Wire Line
	14250 7750 13750 7750
Wire Wire Line
	14250 7650 13750 7650
Wire Wire Line
	14250 7950 13750 7950
Wire Wire Line
	14250 8050 13750 8050
Wire Wire Line
	14250 8150 13750 8150
Wire Wire Line
	15750 7450 15250 7450
Wire Wire Line
	15750 7750 15250 7750
Wire Wire Line
	15750 7950 15250 7950
Wire Wire Line
	15750 8050 15250 8050
Wire Wire Line
	15750 8150 15250 8150
Text Label 13750 7450 0    50   ~ 0
VIN_7
Text Label 15750 7950 2    50   ~ 0
GND
Text Label 15750 8050 2    50   ~ 0
GND
Text Label 15750 8150 2    50   ~ 0
GND
Text Label 13750 8150 0    50   ~ 0
GND
Text Label 13750 8050 0    50   ~ 0
GND
Text Label 13750 7950 0    50   ~ 0
GND
Text Label 13750 7650 0    50   ~ 0
NTC7
Text Label 13750 7750 0    50   ~ 0
NTC_RET7
Wire Wire Line
	15750 7650 15250 7650
Wire Wire Line
	15750 7550 15250 7550
$Comp
L multimodule_power_adaptercard-rescue:FTM-108-02-L-DV-FTM-108-02-L-DV Module4
U 1 1 6489F044
P 11100 9200
F 0 "Module4" H 11100 9767 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 11100 9676 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 11100 9200 50  0001 L BNN
F 3 "E" H 11100 9200 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 11100 9200 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 11100 9200 50  0001 L BNN "Field5"
	1    11100 9200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 8900 10100 8900
Wire Wire Line
	10600 9000 10100 9000
Wire Wire Line
	10600 9200 10100 9200
Wire Wire Line
	10600 9100 10100 9100
Wire Wire Line
	10600 9400 10100 9400
Wire Wire Line
	10600 9500 10100 9500
Wire Wire Line
	10600 9600 10100 9600
Wire Wire Line
	12100 8900 11600 8900
Wire Wire Line
	12100 9200 11600 9200
Wire Wire Line
	12100 9400 11600 9400
Wire Wire Line
	12100 9500 11600 9500
Wire Wire Line
	12100 9600 11600 9600
Text Label 10100 8900 0    50   ~ 0
VIN_4
Text Label 12100 9400 2    50   ~ 0
GND
Text Label 12100 9500 2    50   ~ 0
GND
Text Label 12100 9600 2    50   ~ 0
GND
Text Label 10100 9600 0    50   ~ 0
GND
Text Label 10100 9500 0    50   ~ 0
GND
Text Label 10100 9400 0    50   ~ 0
GND
Text Label 10100 9100 0    50   ~ 0
NTC4
Text Label 10100 9200 0    50   ~ 0
NTC_RET4
Wire Wire Line
	12100 9100 11600 9100
$Comp
L multimodule_power_adaptercard-rescue:FTM-108-02-L-DV-FTM-108-02-L-DV Module5
U 1 1 648B09AA
P 14850 4800
F 0 "Module5" H 14850 5367 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 14850 5276 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 14850 4800 50  0001 L BNN
F 3 "E" H 14850 4800 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 14850 4800 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 14850 4800 50  0001 L BNN "Field5"
	1    14850 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	14350 4800 13850 4800
Wire Wire Line
	14350 4700 13850 4700
Wire Wire Line
	14350 5000 13850 5000
Wire Wire Line
	14350 5100 13850 5100
Wire Wire Line
	14350 5200 13850 5200
Wire Wire Line
	15850 4500 15350 4500
Wire Wire Line
	15850 4800 15350 4800
Wire Wire Line
	15850 5000 15350 5000
Wire Wire Line
	15850 5100 15350 5100
Wire Wire Line
	15850 5200 15350 5200
Text Label 15850 5000 2    50   ~ 0
GND
Text Label 15850 5100 2    50   ~ 0
GND
Text Label 15850 5200 2    50   ~ 0
GND
Text Label 13850 5200 0    50   ~ 0
GND
Text Label 13850 5100 0    50   ~ 0
GND
Text Label 13850 5000 0    50   ~ 0
GND
Text Label 13850 4700 0    50   ~ 0
NTC5
Text Label 13850 4800 0    50   ~ 0
NTC_RET5
Wire Wire Line
	15850 4700 15350 4700
Wire Wire Line
	15850 4600 15350 4600
$Comp
L multimodule_power_adaptercard-rescue:FTM-108-02-L-DV-FTM-108-02-L-DV Module8
U 1 1 648E5BAB
P 14700 9100
F 0 "Module8" H 14700 9667 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 14700 9576 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 14700 9100 50  0001 L BNN
F 3 "E" H 14700 9100 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 14700 9100 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 14700 9100 50  0001 L BNN "Field5"
	1    14700 9100
	1    0    0    -1  
$EndComp
Wire Wire Line
	14200 8800 13700 8800
Wire Wire Line
	14200 8900 13700 8900
Wire Wire Line
	14200 9100 13700 9100
Wire Wire Line
	14200 9000 13700 9000
Wire Wire Line
	14200 9300 13700 9300
Wire Wire Line
	14200 9400 13700 9400
Wire Wire Line
	14200 9500 13700 9500
Wire Wire Line
	15700 8800 15200 8800
Wire Wire Line
	15700 9100 15200 9100
Wire Wire Line
	15700 9300 15200 9300
Wire Wire Line
	15700 9400 15200 9400
Wire Wire Line
	15700 9500 15200 9500
Text Label 13700 8800 0    50   ~ 0
VIN_8
Text Label 15700 9300 2    50   ~ 0
GND
Text Label 15700 9400 2    50   ~ 0
GND
Text Label 15700 9500 2    50   ~ 0
GND
Text Label 13700 9500 0    50   ~ 0
GND
Text Label 13700 9400 0    50   ~ 0
GND
Text Label 13700 9300 0    50   ~ 0
GND
Text Label 13700 9000 0    50   ~ 0
NTC8
Text Label 13700 9100 0    50   ~ 0
NTC_RET8
Wire Wire Line
	15700 9000 15200 9000
Wire Wire Line
	15700 8900 15200 8900
$Comp
L Connector_Generic:Conn_01x03 NTC2
U 1 1 6497A2F1
P 12150 2800
F 0 "NTC2" H 12230 2842 50  0000 L CNN
F 1 "Conn_01x03" H 12230 2751 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 12150 2800 50  0001 C CNN
F 3 "~" H 12150 2800 50  0001 C CNN
	1    12150 2800
	1    0    0    -1  
$EndComp
Text Label 11950 2700 2    50   ~ 0
NTC2
$Comp
L Device:R R2
U 1 1 6497A2FA
P 9950 6200
F 0 "R2" V 9743 6200 50  0000 C CNN
F 1 "50k" V 9834 6200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 9880 6200 50  0001 C CNN
F 3 "~" H 9950 6200 50  0001 C CNN
	1    9950 6200
	0    1    1    0   
$EndComp
Text Label 11950 2800 2    50   ~ 0
NTC_RET2
Text Label 13300 2700 2    50   ~ 0
NTC3
$Comp
L Device:R R3
U 1 1 6498A917
P 10050 7650
F 0 "R3" V 9843 7650 50  0000 C CNN
F 1 "50k" V 9934 7650 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 9980 7650 50  0001 C CNN
F 3 "~" H 10050 7650 50  0001 C CNN
	1    10050 7650
	0    1    1    0   
$EndComp
Text Label 13300 2800 2    50   ~ 0
NTC_RET3
$Comp
L Device:R R4
U 1 1 6499BB2C
P 9950 9200
F 0 "R4" V 9743 9200 50  0000 C CNN
F 1 "50k" V 9834 9200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 9880 9200 50  0001 C CNN
F 3 "~" H 9950 9200 50  0001 C CNN
	1    9950 9200
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 NTC5
U 1 1 649ADA0A
P 10850 3250
F 0 "NTC5" H 10930 3292 50  0000 L CNN
F 1 "Conn_01x03" H 10930 3201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10850 3250 50  0001 C CNN
F 3 "~" H 10850 3250 50  0001 C CNN
	1    10850 3250
	1    0    0    -1  
$EndComp
Text Label 10650 3150 2    50   ~ 0
NTC5
$Comp
L Device:R R5
U 1 1 649ADA13
P 13700 4800
F 0 "R5" V 13493 4800 50  0000 C CNN
F 1 "50k" V 13584 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 13630 4800 50  0001 C CNN
F 3 "~" H 13700 4800 50  0001 C CNN
	1    13700 4800
	0    1    1    0   
$EndComp
Text Label 10650 3250 2    50   ~ 0
NTC_RET5
$Comp
L Connector_Generic:Conn_01x03 NTC6
U 1 1 649C0553
P 12150 3250
F 0 "NTC6" H 12230 3292 50  0000 L CNN
F 1 "Conn_01x03" H 12230 3201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 12150 3250 50  0001 C CNN
F 3 "~" H 12150 3250 50  0001 C CNN
	1    12150 3250
	1    0    0    -1  
$EndComp
Text Label 11950 3150 2    50   ~ 0
NTC6
$Comp
L Device:R R6
U 1 1 649C055C
P 13650 6200
F 0 "R6" V 13443 6200 50  0000 C CNN
F 1 "50k" V 13534 6200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 13580 6200 50  0001 C CNN
F 3 "~" H 13650 6200 50  0001 C CNN
	1    13650 6200
	0    1    1    0   
$EndComp
Text Label 11950 3250 2    50   ~ 0
NTC_RET6
$Comp
L Connector_Generic:Conn_01x03 NTC7
U 1 1 649D3CCA
P 13500 3250
F 0 "NTC7" H 13580 3292 50  0000 L CNN
F 1 "Conn_01x03" H 13580 3201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 13500 3250 50  0001 C CNN
F 3 "~" H 13500 3250 50  0001 C CNN
	1    13500 3250
	1    0    0    -1  
$EndComp
Text Label 13300 3150 2    50   ~ 0
NTC7
$Comp
L Device:R R7
U 1 1 649D3CD3
P 13600 7750
F 0 "R7" V 13393 7750 50  0000 C CNN
F 1 "50k" V 13484 7750 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 13530 7750 50  0001 C CNN
F 3 "~" H 13600 7750 50  0001 C CNN
	1    13600 7750
	0    1    1    0   
$EndComp
Text Label 13300 3250 2    50   ~ 0
NTC_RET7
$Comp
L Connector_Generic:Conn_01x03 NTC8
U 1 1 649E7F71
P 14750 3250
F 0 "NTC8" H 14830 3292 50  0000 L CNN
F 1 "Conn_01x03" H 14830 3201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 14750 3250 50  0001 C CNN
F 3 "~" H 14750 3250 50  0001 C CNN
	1    14750 3250
	1    0    0    -1  
$EndComp
Text Label 14550 3150 2    50   ~ 0
NTC8
$Comp
L Device:R R8
U 1 1 649E7F7A
P 13550 9100
F 0 "R8" V 13343 9100 50  0000 C CNN
F 1 "50k" V 13434 9100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 13480 9100 50  0001 C CNN
F 3 "~" H 13550 9100 50  0001 C CNN
	1    13550 9100
	0    1    1    0   
$EndComp
Text Label 14550 3250 2    50   ~ 0
NTC_RET8
NoConn ~ 11500 4850
NoConn ~ 10500 4850
NoConn ~ 15200 9200
NoConn ~ 14200 9200
NoConn ~ 15350 4900
NoConn ~ 14350 4900
NoConn ~ 10600 9300
NoConn ~ 11600 9300
NoConn ~ 15250 7850
NoConn ~ 14250 7850
NoConn ~ 11700 7750
NoConn ~ 10700 7750
NoConn ~ 11600 6300
NoConn ~ 10600 6300
NoConn ~ 14300 6300
NoConn ~ 15300 6300
$Comp
L multimodule_power_adaptercard-rescue:FTM-108-02-L-DV-FTM-108-02-L-DV Module1
U 1 1 600C66C4
P 11000 4750
F 0 "Module1" H 11000 5317 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 11000 5226 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 11000 4750 50  0001 L BNN
F 3 "E" H 11000 4750 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 11000 4750 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 11000 4750 50  0001 L BNN "Field5"
	1    11000 4750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 64CA06E7
P 900 10650
F 0 "H1" H 1000 10696 50  0000 L CNN
F 1 "MountingHole" H 1000 10605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 900 10650 50  0001 C CNN
F 3 "~" H 900 10650 50  0001 C CNN
	1    900  10650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 64CA1060
P 1650 10650
F 0 "H2" H 1750 10696 50  0000 L CNN
F 1 "MountingHole" H 1750 10605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1650 10650 50  0001 C CNN
F 3 "~" H 1650 10650 50  0001 C CNN
	1    1650 10650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 64CA1302
P 2500 10650
F 0 "H3" H 2600 10696 50  0000 L CNN
F 1 "MountingHole" H 2600 10605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2500 10650 50  0001 C CNN
F 3 "~" H 2500 10650 50  0001 C CNN
	1    2500 10650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 64CA15A7
P 3300 10650
F 0 "H4" H 3400 10696 50  0000 L CNN
F 1 "MountingHole" H 3400 10605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 3300 10650 50  0001 C CNN
F 3 "~" H 3300 10650 50  0001 C CNN
	1    3300 10650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 64CA1840
P 4100 10650
F 0 "H5" H 4200 10696 50  0000 L CNN
F 1 "MountingHole" H 4200 10605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 4100 10650 50  0001 C CNN
F 3 "~" H 4100 10650 50  0001 C CNN
	1    4100 10650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 64CA1BFE
P 5100 10650
F 0 "H6" H 5200 10696 50  0000 L CNN
F 1 "MountingHole" H 5200 10605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5100 10650 50  0001 C CNN
F 3 "~" H 5100 10650 50  0001 C CNN
	1    5100 10650
	1    0    0    -1  
$EndComp
Text Label 10650 2900 2    50   ~ 0
NTC+5_1
Text Label 11950 2900 2    50   ~ 0
NTC+5_2
Text Label 10650 3350 2    50   ~ 0
NTC+5_5
Text Label 11950 3350 2    50   ~ 0
NTC+5_6
Text Label 14550 3350 2    50   ~ 0
NTC+5_8
Text Label 13300 3350 2    50   ~ 0
NTC+5_7
Text Label 13300 2900 2    50   ~ 0
NTC+5_3
Text Label 9700 4750 2    50   ~ 0
NTC+5_1
Text Label 13550 4800 2    50   ~ 0
NTC+5_5
Text Label 9900 7650 2    50   ~ 0
NTC+5_3
Text Label 9800 6200 2    50   ~ 0
NTC+5_2
Text Label 13500 6200 2    50   ~ 0
NTC+5_6
Text Label 13450 7750 2    50   ~ 0
NTC+5_7
Text Label 13400 9100 2    50   ~ 0
NTC+5_8
Text Label 9800 9200 2    50   ~ 0
NTC+5_4
$Comp
L Connector_Generic:Conn_01x02 J_M_NTC_5
U 1 1 64CB31EA
P 13250 5000
F 0 "J_M_NTC_5" H 13330 4992 50  0000 L CNN
F 1 "Conn_01x02" H 13330 4901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 13250 5000 50  0001 C CNN
F 3 "~" H 13250 5000 50  0001 C CNN
	1    13250 5000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_M_NTC_1
U 1 1 64CB63EA
P 9450 4950
F 0 "J_M_NTC_1" H 9530 4942 50  0000 L CNN
F 1 "Conn_01x02" H 9530 4851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9450 4950 50  0001 C CNN
F 3 "~" H 9450 4950 50  0001 C CNN
	1    9450 4950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_M_NTC_4
U 1 1 64CB75A5
P 9500 9400
F 0 "J_M_NTC_4" H 9580 9392 50  0000 L CNN
F 1 "Conn_01x02" H 9580 9301 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9500 9400 50  0001 C CNN
F 3 "~" H 9500 9400 50  0001 C CNN
	1    9500 9400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_M_NTC_3
U 1 1 64CB89AC
P 9600 7900
F 0 "J_M_NTC_3" H 9680 7892 50  0000 L CNN
F 1 "Conn_01x02" H 9680 7801 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9600 7900 50  0001 C CNN
F 3 "~" H 9600 7900 50  0001 C CNN
	1    9600 7900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_M_NTC_7
U 1 1 64CB8F61
P 13150 7900
F 0 "J_M_NTC_7" H 13230 7892 50  0000 L CNN
F 1 "Conn_01x02" H 13230 7801 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 13150 7900 50  0001 C CNN
F 3 "~" H 13150 7900 50  0001 C CNN
	1    13150 7900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_M_NTC_8
U 1 1 64CB94E5
P 13100 9350
F 0 "J_M_NTC_8" H 13180 9342 50  0000 L CNN
F 1 "Conn_01x02" H 13180 9251 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 13100 9350 50  0001 C CNN
F 3 "~" H 13100 9350 50  0001 C CNN
	1    13100 9350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_M_NTC_6
U 1 1 64CB9EB7
P 13100 6350
F 0 "J_M_NTC_6" H 13180 6342 50  0000 L CNN
F 1 "Conn_01x02" H 13180 6251 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 13100 6350 50  0001 C CNN
F 3 "~" H 13100 6350 50  0001 C CNN
	1    13100 6350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_M_NTC_2
U 1 1 64CBAED4
P 9350 6350
F 0 "J_M_NTC_2" H 9430 6342 50  0000 L CNN
F 1 "Conn_01x02" H 9430 6251 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9350 6350 50  0001 C CNN
F 3 "~" H 9350 6350 50  0001 C CNN
	1    9350 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 4950 9250 4750
Wire Wire Line
	9250 4750 9700 4750
Wire Wire Line
	12900 9350 13400 9350
Wire Wire Line
	13400 9350 13400 9100
Wire Wire Line
	13050 5000 13550 5000
Wire Wire Line
	13550 5000 13550 4800
Wire Wire Line
	9300 9400 9300 9200
Wire Wire Line
	9300 9200 9800 9200
Wire Wire Line
	9400 7900 9900 7900
Wire Wire Line
	9900 7900 9900 7650
Wire Wire Line
	9150 6350 9150 6200
Wire Wire Line
	9150 6200 9800 6200
Wire Wire Line
	12900 6350 12900 6200
Wire Wire Line
	12900 6200 13500 6200
Wire Wire Line
	12950 7900 12950 7750
Wire Wire Line
	12950 7750 13450 7750
Text Label 12900 9450 2    50   ~ 0
NTC+5
Text Label 9250 5050 2    50   ~ 0
NTC+5
Text Label 13050 5100 2    50   ~ 0
NTC+5
Text Label 12950 8000 2    50   ~ 0
NTC+5
Text Label 9300 9500 2    50   ~ 0
NTC+5
Text Label 9400 8000 2    50   ~ 0
NTC+5
Text Label 9150 6450 2    50   ~ 0
NTC+5
Text Label 12900 6450 2    50   ~ 0
NTC+5
$Comp
L Connector_Generic:Conn_02x04_Odd_Even ChuckNTC1-4
U 1 1 64B580C9
P 1850 1200
F 0 "ChuckNTC1-4" H 1900 1517 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 1900 1426 50  0000 C CNN
F 2 "Library:phoenix4p45deg" H 1850 1200 50  0001 C CNN
F 3 "~" H 1850 1200 50  0001 C CNN
	1    1850 1200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even ChuckNTC5-8
U 1 1 64B5896F
P 3400 1200
F 0 "ChuckNTC5-8" H 3450 1517 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 3450 1426 50  0000 C CNN
F 2 "Library:phoenix4p45deg" H 3400 1200 50  0001 C CNN
F 3 "~" H 3400 1200 50  0001 C CNN
	1    3400 1200
	1    0    0    -1  
$EndComp
Text Label 2150 1100 0    50   ~ 0
NTC_CH_RET1
Text Label 2150 1200 0    50   ~ 0
NTC_CH_RET2
Text Label 2150 1300 0    50   ~ 0
NTC_CH_RET3
Text Label 2150 1400 0    50   ~ 0
NTC_CH_RET4
Text Label 3700 1100 0    50   ~ 0
NTC_CH_RET5
Text Label 3700 1200 0    50   ~ 0
NTC_CH_RET6
Text Label 3700 1400 0    50   ~ 0
NTC_CH_RET8
Text Label 3700 1300 0    50   ~ 0
NTC_CH_RET7
$Comp
L Connector_Generic:Conn_01x02 Input_NTC_1
U 1 1 64B0D3AD
P 3450 2500
F 0 "Input_NTC_1" H 3530 2492 50  0000 L CNN
F 1 "Conn_01x02" H 3530 2401 50  0000 L CNN
F 2 "WR-TBL3470B:691347000002B" H 3450 2500 50  0001 C CNN
F 3 "~" H 3450 2500 50  0001 C CNN
	1    3450 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 Input_NTC_2
U 1 1 64B0EA16
P 3400 3100
F 0 "Input_NTC_2" H 3480 3092 50  0000 L CNN
F 1 "Conn_01x02" H 3480 3001 50  0000 L CNN
F 2 "WR-TBL3470B:691347000002B" H 3400 3100 50  0001 C CNN
F 3 "~" H 3400 3100 50  0001 C CNN
	1    3400 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 Input_NTC_3
U 1 1 64B0ED1A
P 3400 3700
F 0 "Input_NTC_3" H 3480 3692 50  0000 L CNN
F 1 "Conn_01x02" H 3480 3601 50  0000 L CNN
F 2 "WR-TBL3470B:691347000002B" H 3400 3700 50  0001 C CNN
F 3 "~" H 3400 3700 50  0001 C CNN
	1    3400 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 Input_NTC_4
U 1 1 64B0F15D
P 3400 4250
F 0 "Input_NTC_4" H 3480 4242 50  0000 L CNN
F 1 "Conn_01x02" H 3480 4151 50  0000 L CNN
F 2 "WR-TBL3470B:691347000002B" H 3400 4250 50  0001 C CNN
F 3 "~" H 3400 4250 50  0001 C CNN
	1    3400 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 Input_NTC_5
U 1 1 64B0F48B
P 3400 4900
F 0 "Input_NTC_5" H 3480 4892 50  0000 L CNN
F 1 "Conn_01x02" H 3480 4801 50  0000 L CNN
F 2 "WR-TBL3470B:691347000002B" H 3400 4900 50  0001 C CNN
F 3 "~" H 3400 4900 50  0001 C CNN
	1    3400 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 Input_NTC_6
U 1 1 64B101A5
P 3400 5600
F 0 "Input_NTC_6" H 3480 5592 50  0000 L CNN
F 1 "Conn_01x02" H 3480 5501 50  0000 L CNN
F 2 "WR-TBL3470B:691347000002B" H 3400 5600 50  0001 C CNN
F 3 "~" H 3400 5600 50  0001 C CNN
	1    3400 5600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 Input_NTC_7
U 1 1 64B1047B
P 3400 6300
F 0 "Input_NTC_7" H 3480 6292 50  0000 L CNN
F 1 "Conn_01x02" H 3480 6201 50  0000 L CNN
F 2 "WR-TBL3470B:691347000002B" H 3400 6300 50  0001 C CNN
F 3 "~" H 3400 6300 50  0001 C CNN
	1    3400 6300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 Input_NTC_8
U 1 1 64B106ED
P 3400 7050
F 0 "Input_NTC_8" H 3480 7042 50  0000 L CNN
F 1 "Conn_01x02" H 3480 6951 50  0000 L CNN
F 2 "WR-TBL3470B:691347000002B" H 3400 7050 50  0001 C CNN
F 3 "~" H 3400 7050 50  0001 C CNN
	1    3400 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 64B23501
P 2750 2600
F 0 "R9" V 2543 2600 50  0000 C CNN
F 1 "10k" V 2634 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2680 2600 50  0001 C CNN
F 3 "~" H 2750 2600 50  0001 C CNN
	1    2750 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 64B23E8F
P 2700 3200
F 0 "R10" V 2493 3200 50  0000 C CNN
F 1 "10k" V 2584 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2630 3200 50  0001 C CNN
F 3 "~" H 2700 3200 50  0001 C CNN
	1    2700 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 64B2429C
P 2700 3800
F 0 "R11" V 2493 3800 50  0000 C CNN
F 1 "10k" V 2584 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2630 3800 50  0001 C CNN
F 3 "~" H 2700 3800 50  0001 C CNN
	1    2700 3800
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 64B244FD
P 2700 4350
F 0 "R12" V 2493 4350 50  0000 C CNN
F 1 "10k" V 2584 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2630 4350 50  0001 C CNN
F 3 "~" H 2700 4350 50  0001 C CNN
	1    2700 4350
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 64B24A5C
P 2700 5000
F 0 "R13" V 2493 5000 50  0000 C CNN
F 1 "10k" V 2584 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2630 5000 50  0001 C CNN
F 3 "~" H 2700 5000 50  0001 C CNN
	1    2700 5000
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 64B252A7
P 2700 5700
F 0 "R14" V 2493 5700 50  0000 C CNN
F 1 "10k" V 2584 5700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2630 5700 50  0001 C CNN
F 3 "~" H 2700 5700 50  0001 C CNN
	1    2700 5700
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 64B2569D
P 2700 6400
F 0 "R15" V 2493 6400 50  0000 C CNN
F 1 "10k" V 2584 6400 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2630 6400 50  0001 C CNN
F 3 "~" H 2700 6400 50  0001 C CNN
	1    2700 6400
	0    1    1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 64B25A60
P 2700 7150
F 0 "R16" V 2493 7150 50  0000 C CNN
F 1 "10k" V 2584 7150 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2630 7150 50  0001 C CNN
F 3 "~" H 2700 7150 50  0001 C CNN
	1    2700 7150
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Chuck_NTC_1
U 1 1 64B26DCC
P 1950 2600
F 0 "J_Chuck_NTC_1" H 2030 2592 50  0000 L CNN
F 1 "Conn_01x02" H 2030 2501 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1950 2600 50  0001 C CNN
F 3 "~" H 1950 2600 50  0001 C CNN
	1    1950 2600
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Chuck_NTC_2
U 1 1 64B2954F
P 1900 3200
F 0 "J_Chuck_NTC_2" H 1980 3192 50  0000 L CNN
F 1 "Conn_01x02" H 1980 3101 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1900 3200 50  0001 C CNN
F 3 "~" H 1900 3200 50  0001 C CNN
	1    1900 3200
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Chuck_NTC_3
U 1 1 64B298BE
P 1850 3800
F 0 "J_Chuck_NTC_3" H 1930 3792 50  0000 L CNN
F 1 "Conn_01x02" H 1930 3701 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1850 3800 50  0001 C CNN
F 3 "~" H 1850 3800 50  0001 C CNN
	1    1850 3800
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Chuck_NTC_4
U 1 1 64B29DB9
P 1850 4350
F 0 "J_Chuck_NTC_4" H 1930 4342 50  0000 L CNN
F 1 "Conn_01x02" H 1930 4251 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1850 4350 50  0001 C CNN
F 3 "~" H 1850 4350 50  0001 C CNN
	1    1850 4350
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Chuck_NTC_5
U 1 1 64B2A456
P 1800 5000
F 0 "J_Chuck_NTC_5" H 1880 4992 50  0000 L CNN
F 1 "Conn_01x02" H 1880 4901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1800 5000 50  0001 C CNN
F 3 "~" H 1800 5000 50  0001 C CNN
	1    1800 5000
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Chuck_NTC_6
U 1 1 64B2A9F4
P 1800 5700
F 0 "J_Chuck_NTC_6" H 1880 5692 50  0000 L CNN
F 1 "Conn_01x02" H 1880 5601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1800 5700 50  0001 C CNN
F 3 "~" H 1800 5700 50  0001 C CNN
	1    1800 5700
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Chuck_NTC_7
U 1 1 64B2AF61
P 1800 6450
F 0 "J_Chuck_NTC_7" H 1880 6442 50  0000 L CNN
F 1 "Conn_01x02" H 1880 6351 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1800 6450 50  0001 C CNN
F 3 "~" H 1800 6450 50  0001 C CNN
	1    1800 6450
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Chuck_NTC_8
U 1 1 64B2B399
P 1750 7200
F 0 "J_Chuck_NTC_8" H 1830 7192 50  0000 L CNN
F 1 "Conn_01x02" H 1830 7101 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1750 7200 50  0001 C CNN
F 3 "~" H 1750 7200 50  0001 C CNN
	1    1750 7200
	-1   0    0    1   
$EndComp
Text Label 1950 7200 0    50   ~ 0
NTC+5
Text Label 2000 6450 0    50   ~ 0
NTC+5
Text Label 2000 5700 0    50   ~ 0
NTC+5
Text Label 2000 5000 0    50   ~ 0
NTC+5
Text Label 2050 4350 0    50   ~ 0
NTC+5
Text Label 2050 3800 0    50   ~ 0
NTC+5
Text Label 2100 3200 0    50   ~ 0
NTC+5
Text Label 2150 2600 0    50   ~ 0
NTC+5
Wire Wire Line
	2150 2500 2600 2500
Wire Wire Line
	2600 2500 2600 2600
Wire Wire Line
	2100 3100 2550 3100
Wire Wire Line
	2550 3100 2550 3200
Wire Wire Line
	2050 3700 2550 3700
Wire Wire Line
	2550 3700 2550 3800
Wire Wire Line
	2050 4250 2550 4250
Wire Wire Line
	2550 4250 2550 4350
Wire Wire Line
	2000 4900 2550 4900
Wire Wire Line
	2550 4900 2550 5000
Wire Wire Line
	2000 5600 2550 5600
Wire Wire Line
	2550 5600 2550 5700
Wire Wire Line
	2000 6350 2550 6350
Wire Wire Line
	2550 6350 2550 6400
Wire Wire Line
	1950 7100 2550 7100
Wire Wire Line
	2550 7100 2550 7150
Wire Wire Line
	1800 9600 3050 9600
Text Label 2500 2500 2    50   ~ 0
NTC+5_ch_1
Text Label 2500 3100 2    50   ~ 0
NTC+5_ch_2
Text Label 2450 3700 2    50   ~ 0
NTC+5_ch_3
Text Label 2500 4250 2    50   ~ 0
NTC+5_ch_4
Text Label 2400 6350 2    50   ~ 0
NTC+5_ch_7
Text Label 2450 7100 2    50   ~ 0
NTC+5_ch_8
Text Label 2500 5600 2    50   ~ 0
NTC+5_ch_6
Text Label 2500 4900 2    50   ~ 0
NTC+5_ch_5
Text Label 3250 2600 2    50   ~ 0
NTC_CH_RET1
Text Label 3200 3200 2    50   ~ 0
NTC_CH_RET2
Text Label 3200 4350 2    50   ~ 0
NTC_CH_RET4
Text Label 3200 3800 2    50   ~ 0
NTC_CH_RET3
Text Label 3200 5700 2    50   ~ 0
NTC_CH_RET6
Text Label 3200 5000 2    50   ~ 0
NTC_CH_RET5
Text Label 3200 6400 2    50   ~ 0
NTC_CH_RET7
Text Label 3200 7150 2    50   ~ 0
NTC_CH_RET8
Wire Wire Line
	3200 1100 3700 1100
Wire Wire Line
	3200 1200 3700 1200
Wire Wire Line
	3200 1300 3700 1300
Wire Wire Line
	3200 1400 3700 1400
Wire Wire Line
	1650 1100 2150 1100
Wire Wire Line
	1650 1200 2150 1200
Wire Wire Line
	1650 1300 2150 1300
Wire Wire Line
	1650 1400 2150 1400
Wire Wire Line
	2900 2600 3250 2600
Text Label 3200 3100 2    50   ~ 0
NTC_CH2
Text Label 3200 3700 2    50   ~ 0
NTC_CH3
Text Label 3200 4250 2    50   ~ 0
NTC_CH4
Text Label 3200 4900 2    50   ~ 0
NTC_CH5
Text Label 3200 5600 2    50   ~ 0
NTC_CH6
Text Label 3200 6300 2    50   ~ 0
NTC_CH7
Text Label 3200 7050 2    50   ~ 0
NTC_CH8
Text Label 3250 2500 2    50   ~ 0
NTC_CH1
Wire Wire Line
	2850 3800 3200 3800
Wire Wire Line
	2850 4350 3200 4350
Wire Wire Line
	2850 5000 3200 5000
Wire Wire Line
	2850 5700 3200 5700
Wire Wire Line
	2850 6400 3200 6400
Wire Wire Line
	2850 7150 3200 7150
Wire Wire Line
	2850 3200 3200 3200
$Comp
L Connector:TestPoint_2Pole TP3
U 1 1 6552032A
P 9850 10800
F 0 "TP3" H 9850 10995 50  0000 C CNN
F 1 "TestPoint_2Pole" H 9850 10904 50  0000 C CNN
F 2 "TestPoint:TestPoint_2Pads_Pitch2.54mm_Drill0.8mm" H 9850 10800 50  0001 C CNN
F 3 "~" H 9850 10800 50  0001 C CNN
	1    9850 10800
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_2Pole TP4
U 1 1 65520AA7
P 10800 10850
F 0 "TP4" H 10800 11045 50  0000 C CNN
F 1 "TestPoint_2Pole" H 10800 10954 50  0000 C CNN
F 2 "TestPoint:TestPoint_2Pads_Pitch2.54mm_Drill0.8mm" H 10800 10850 50  0001 C CNN
F 3 "~" H 10800 10850 50  0001 C CNN
	1    10800 10850
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_2Pole TP2
U 1 1 65520E53
P 7750 10800
F 0 "TP2" H 7750 10995 50  0000 C CNN
F 1 "TestPoint_2Pole" H 7750 10904 50  0000 C CNN
F 2 "TestPoint:TestPoint_2Pads_Pitch2.54mm_Drill0.8mm" H 7750 10800 50  0001 C CNN
F 3 "~" H 7750 10800 50  0001 C CNN
	1    7750 10800
	1    0    0    -1  
$EndComp
NoConn ~ 11500 4450
NoConn ~ 11500 4550
NoConn ~ 11500 4650
NoConn ~ 11500 4750
NoConn ~ 10500 4450
NoConn ~ 10500 4550
NoConn ~ 15850 4500
NoConn ~ 15850 4600
NoConn ~ 15850 4700
NoConn ~ 15850 4800
NoConn ~ 14350 4500
NoConn ~ 14350 4600
NoConn ~ 10100 5900
NoConn ~ 10100 6000
NoConn ~ 11600 5900
NoConn ~ 12100 6000
NoConn ~ 12100 6100
NoConn ~ 12100 6200
NoConn ~ 14300 5900
NoConn ~ 14300 6000
NoConn ~ 15300 5900
NoConn ~ 15300 6000
NoConn ~ 15300 6100
NoConn ~ 15300 6200
Text Label 10200 7450 0    50   ~ 0
VIN_3
Text Label 11700 7350 0    50   ~ 0
VIN_3
Text Label 12200 7450 0    50   ~ 0
VIN_3
Text Label 12200 7550 0    50   ~ 0
VIN_3
Text Label 12200 7650 0    50   ~ 0
VIN_3
Text Label 10100 9000 0    50   ~ 0
VIN_4
Text Label 12100 8900 0    50   ~ 0
VIN_4
Text Label 11600 9000 0    50   ~ 0
VIN_4
Text Label 12100 9100 0    50   ~ 0
VIN_4
Text Label 12100 9200 0    50   ~ 0
VIN_4
Text Label 7550 10800 0    50   ~ 0
VIN_4
Text Label 13750 7550 0    50   ~ 0
VIN_7
Text Label 15750 7450 0    50   ~ 0
VIN_7
Text Label 15750 7550 0    50   ~ 0
VIN_7
Text Label 15750 7650 0    50   ~ 0
VIN_7
Text Label 15750 7750 0    50   ~ 0
VIN_7
Text Label 10050 10800 0    50   ~ 0
VIN_7
Text Label 9650 10800 0    50   ~ 0
VIN_7
Text Label 13700 8900 0    50   ~ 0
VIN_8
Text Label 15700 8800 0    50   ~ 0
VIN_8
Text Label 15700 8900 0    50   ~ 0
VIN_8
Text Label 15700 9000 0    50   ~ 0
VIN_8
Text Label 15700 9100 0    50   ~ 0
VIN_8
Text Label 11000 10850 0    50   ~ 0
VIN_8
Text Label 10600 10850 0    50   ~ 0
VIN_8
$Comp
L Mechanical:MountingHole H8
U 1 1 657A806D
P 1650 10900
F 0 "H8" H 1750 10946 50  0000 L CNN
F 1 "MountingHole" H 1750 10855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1650 10900 50  0001 C CNN
F 3 "~" H 1650 10900 50  0001 C CNN
	1    1650 10900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H7
U 1 1 657A8325
P 900 10950
F 0 "H7" H 1000 10996 50  0000 L CNN
F 1 "MountingHole" H 1000 10905 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 900 10950 50  0001 C CNN
F 3 "~" H 900 10950 50  0001 C CNN
	1    900  10950
	1    0    0    -1  
$EndComp
Text Label 7950 10800 0    50   ~ 0
VIN_4
Text Label 8600 10800 0    50   ~ 0
VIN_3
Text Label 9000 10800 0    50   ~ 0
VIN_3
$Comp
L Connector:TestPoint_2Pole TP1
U 1 1 65519694
P 8800 10800
F 0 "TP1" H 8800 10995 50  0000 C CNN
F 1 "TestPoint_2Pole" H 8800 10904 50  0000 C CNN
F 2 "TestPoint:TestPoint_2Pads_Pitch2.54mm_Drill0.8mm" H 8800 10800 50  0001 C CNN
F 3 "~" H 8800 10800 50  0001 C CNN
	1    8800 10800
	1    0    0    -1  
$EndComp
Text Label 14550 2900 2    50   ~ 0
NTC+5_4
Text Label 14550 2800 2    50   ~ 0
NTC_RET4
Text Label 14550 2700 2    50   ~ 0
NTC4
$Comp
L Connector_Generic:Conn_01x03 NTC4
U 1 1 6499BB23
P 14750 2800
F 0 "NTC4" H 14830 2842 50  0000 L CNN
F 1 "Conn_01x03" H 14830 2751 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 14750 2800 50  0001 C CNN
F 3 "~" H 14750 2800 50  0001 C CNN
	1    14750 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 NTC3
U 1 1 6498A90E
P 13500 2800
F 0 "NTC3" H 13580 2842 50  0000 L CNN
F 1 "Conn_01x03" H 13580 2751 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 13500 2800 50  0001 C CNN
F 3 "~" H 13500 2800 50  0001 C CNN
	1    13500 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 9400 2950 9400
Connection ~ 3050 9400
Wire Wire Line
	3050 9600 3050 9400
Wire Wire Line
	1800 9400 1800 9600
Wire Wire Line
	3000 9100 3100 9100
Connection ~ 3000 9100
Wire Wire Line
	1800 8800 1800 9100
Wire Wire Line
	3000 8800 1800 8800
Wire Wire Line
	3000 9100 3000 8800
Wire Wire Line
	2900 9100 3000 9100
Wire Wire Line
	2350 9200 1800 9200
Connection ~ 2350 9200
Wire Wire Line
	2350 9400 2350 9200
Wire Wire Line
	2650 9400 2350 9400
Wire Wire Line
	3100 9300 1800 9300
Wire Wire Line
	2600 9200 2350 9200
Connection ~ 2600 9200
Wire Wire Line
	2600 9100 2600 9200
Wire Wire Line
	3100 9200 2600 9200
Wire Wire Line
	3100 9400 3050 9400
$Comp
L Device:R R18
U 1 1 64C434C1
P 2800 9400
F 0 "R18" V 2593 9400 50  0000 C CNN
F 1 "10k" V 2684 9400 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2730 9400 50  0001 C CNN
F 3 "~" H 2800 9400 50  0001 C CNN
	1    2800 9400
	0    1    1    0   
$EndComp
$Comp
L Device:R R17
U 1 1 64C42C05
P 2750 9100
F 0 "R17" V 2543 9100 50  0000 C CNN
F 1 "10k" V 2634 9100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2680 9100 50  0001 C CNN
F 3 "~" H 2750 9100 50  0001 C CNN
	1    2750 9100
	0    1    1    0   
$EndComp
Text Label 6250 9400 2    50   ~ 0
SHT85_SDA
Text Label 6250 9300 2    50   ~ 0
SHT85_VSS
Text Label 5200 9300 0    50   ~ 0
SHT85_VSS
Text Label 6250 9200 2    50   ~ 0
SHT85_VDD
Text Label 6250 9100 2    50   ~ 0
SHT85_SCL
Wire Wire Line
	4700 9400 4750 9400
Connection ~ 4700 9400
Wire Wire Line
	4700 9550 4700 9400
Wire Wire Line
	5200 9550 4700 9550
Wire Wire Line
	5200 9400 5200 9550
Wire Wire Line
	4700 9100 4750 9100
Connection ~ 4700 9100
Wire Wire Line
	4700 8800 4700 9100
Wire Wire Line
	5200 8800 4700 8800
Wire Wire Line
	5200 9100 5200 8800
Wire Wire Line
	4600 9400 4700 9400
Wire Wire Line
	4600 9100 4700 9100
Wire Wire Line
	4600 9200 5050 9200
Wire Wire Line
	4600 9300 5200 9300
Wire Wire Line
	5200 9200 5050 9200
Text Label 5200 9400 0    50   ~ 0
SHT85_SDA
Text Label 5200 9100 0    50   ~ 0
SHT85_SCL
Text Label 5200 9200 0    50   ~ 0
SHT85_VDD
Connection ~ 5050 9200
Wire Wire Line
	5050 9400 5050 9200
Wire Wire Line
	5050 9200 5050 9100
$Comp
L Device:R R20
U 1 1 64BA37A0
P 4900 9400
F 0 "R20" V 4693 9400 50  0000 C CNN
F 1 "10k" V 4784 9400 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4830 9400 50  0001 C CNN
F 3 "~" H 4900 9400 50  0001 C CNN
	1    4900 9400
	0    1    1    0   
$EndComp
$Comp
L Device:R R19
U 1 1 64BA2C85
P 4900 9100
F 0 "R19" V 4693 9100 50  0000 C CNN
F 1 "10k" V 4784 9100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4830 9100 50  0001 C CNN
F 3 "~" H 4900 9100 50  0001 C CNN
	1    4900 9100
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 64B9EB6B
P 4400 9200
F 0 "J1" H 4508 9481 50  0000 C CNN
F 1 "SHT85" H 4508 9390 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 4400 9200 50  0001 C CNN
F 3 "~" H 4400 9200 50  0001 C CNN
	1    4400 9200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J_SHT_OUT1
U 1 1 64B8F877
P 1600 9200
F 0 "J_SHT_OUT1" H 1680 9192 50  0000 L CNN
F 1 "Conn_01x04" H 1680 9101 50  0000 L CNN
F 2 "WR-TBL3470B:691347000004B" H 1600 9200 50  0001 C CNN
F 3 "~" H 1600 9200 50  0001 C CNN
	1    1600 9200
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J_SHT_OUT2
U 1 1 64B8F44B
P 6450 9200
F 0 "J_SHT_OUT2" H 6530 9192 50  0000 L CNN
F 1 "Conn_01x04" H 6530 9101 50  0000 L CNN
F 2 "WR-TBL3470B:691347000004B" H 6450 9200 50  0001 C CNN
F 3 "~" H 6450 9200 50  0001 C CNN
	1    6450 9200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J_SHT_IN1
U 1 1 64DCBF73
P 3300 9200
F 0 "J_SHT_IN1" H 3380 9192 50  0000 L CNN
F 1 "Conn_01x04" H 3380 9101 50  0000 L CNN
F 2 "WR-TBL3470B:691347000004B" H 3300 9200 50  0001 C CNN
F 3 "~" H 3300 9200 50  0001 C CNN
	1    3300 9200
	1    0    0    -1  
$EndComp
Wire Wire Line
	14750 1750 15250 1750
Wire Wire Line
	14750 1650 15250 1650
Wire Wire Line
	14750 1550 15250 1550
Wire Wire Line
	14750 1450 15250 1450
Wire Wire Line
	13300 1500 13800 1500
Wire Wire Line
	13300 1600 13800 1600
Wire Wire Line
	13300 1700 13800 1700
Wire Wire Line
	13300 1800 13800 1800
Text Label 8750 1300 2    50   ~ 0
NTC+5
Text Label 9250 1300 0    50   ~ 0
NTC+5
Text Label 8750 1400 2    50   ~ 0
GND
Text Label 9250 1400 0    50   ~ 0
GND
Text Label 15250 1450 0    50   ~ 0
NTC_RET8
Text Label 15250 1550 0    50   ~ 0
NTC_RET7
Text Label 15250 1650 0    50   ~ 0
NTC_RET6
Text Label 15250 1750 0    50   ~ 0
NTC_RET5
Text Label 13800 1800 0    50   ~ 0
NTC_RET4
Text Label 13800 1700 0    50   ~ 0
NTC_RET3
Text Label 13800 1600 0    50   ~ 0
NTC_RET2
Text Label 13800 1500 0    50   ~ 0
NTC_RET1
$Comp
L Connector_Generic:Conn_02x04_Odd_Even ModuleNTC5-8
U 1 1 64CCBF89
P 14950 1550
F 0 "ModuleNTC5-8" H 15000 1867 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 15000 1776 50  0000 C CNN
F 2 "Library:1017505" H 14950 1550 50  0001 C CNN
F 3 "~" H 14950 1550 50  0001 C CNN
	1    14950 1550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J5V1
U 1 1 64CAE139
P 8950 1300
F 0 "J5V1" H 9000 1517 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 9000 1426 50  0000 C CNN
F 2 "Library:1017503" H 8950 1300 50  0001 C CNN
F 3 "~" H 8950 1300 50  0001 C CNN
	1    8950 1300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even ModuleNTC1-4
U 1 1 64CA917D
P 13500 1600
F 0 "ModuleNTC1-4" H 13550 1917 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 13550 1826 50  0000 C CNN
F 2 "Library:1017505" H 13500 1600 50  0001 C CNN
F 3 "~" H 13500 1600 50  0001 C CNN
	1    13500 1600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 NTC_Chuck1
U 1 1 64D57904
P 6100 2550
F 0 "NTC_Chuck1" H 6180 2592 50  0000 L CNN
F 1 "Conn_01x03" H 6180 2501 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6100 2550 50  0001 C CNN
F 3 "~" H 6100 2550 50  0001 C CNN
	1    6100 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2450 5550 2450
Text Label 5900 2550 2    50   ~ 0
NTC_CH_RET1
$Comp
L Connector_Generic:Conn_01x03 NTC_Chuck4
U 1 1 64D63D3F
P 6150 4250
F 0 "NTC_Chuck4" H 6230 4292 50  0000 L CNN
F 1 "Conn_01x03" H 6230 4201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6150 4250 50  0001 C CNN
F 3 "~" H 6150 4250 50  0001 C CNN
	1    6150 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4150 5600 4150
Text Label 5600 4150 0    50   ~ 0
NTC_CH4
Text Label 5950 4250 2    50   ~ 0
NTC_CH_RET4
$Comp
L Connector_Generic:Conn_01x03 NTC_Chuck5
U 1 1 64D6FD30
P 6100 4900
F 0 "NTC_Chuck5" H 6180 4942 50  0000 L CNN
F 1 "Conn_01x03" H 6180 4851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6100 4900 50  0001 C CNN
F 3 "~" H 6100 4900 50  0001 C CNN
	1    6100 4900
	1    0    0    -1  
$EndComp
Text Label 5900 4800 2    50   ~ 0
NTC_CH5
Text Label 5900 4900 2    50   ~ 0
NTC_CH_RET5
$Comp
L Connector_Generic:Conn_01x03 NTC_Chuck7
U 1 1 64D7C521
P 6100 6300
F 0 "NTC_Chuck7" H 6180 6342 50  0000 L CNN
F 1 "Conn_01x03" H 6180 6251 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6100 6300 50  0001 C CNN
F 3 "~" H 6100 6300 50  0001 C CNN
	1    6100 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 6200 5550 6200
Text Label 5550 6200 0    50   ~ 0
NTC_CH7
Text Label 5900 6300 2    50   ~ 0
NTC_CH_RET7
$Comp
L Connector_Generic:Conn_01x03 NTC_Chuck2
U 1 1 64D89A3A
P 6100 3150
F 0 "NTC_Chuck2" H 6180 3192 50  0000 L CNN
F 1 "Conn_01x03" H 6180 3101 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6100 3150 50  0001 C CNN
F 3 "~" H 6100 3150 50  0001 C CNN
	1    6100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3050 5550 3050
Text Label 5550 3050 0    50   ~ 0
NTC_CH2
Text Label 5900 3150 2    50   ~ 0
NTC_CH_RET2
$Comp
L Connector_Generic:Conn_01x03 NTC_Chuck3
U 1 1 64D97911
P 6100 3700
F 0 "NTC_Chuck3" H 6180 3742 50  0000 L CNN
F 1 "Conn_01x03" H 6180 3651 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6100 3700 50  0001 C CNN
F 3 "~" H 6100 3700 50  0001 C CNN
	1    6100 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3600 5550 3600
Text Label 5550 3600 0    50   ~ 0
NTC_CH3
Text Label 5900 3700 2    50   ~ 0
NTC_CH_RET3
$Comp
L Connector_Generic:Conn_01x03 NTC_Chuck6
U 1 1 64DA5D8A
P 6100 5600
F 0 "NTC_Chuck6" H 6180 5642 50  0000 L CNN
F 1 "Conn_01x03" H 6180 5551 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6100 5600 50  0001 C CNN
F 3 "~" H 6100 5600 50  0001 C CNN
	1    6100 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 5500 5550 5500
Text Label 5550 5500 0    50   ~ 0
NTC_CH6
Text Label 5900 5600 2    50   ~ 0
NTC_CH_RET6
$Comp
L Connector_Generic:Conn_01x03 NTC_Chuck8
U 1 1 64DB5057
P 6150 7100
F 0 "NTC_Chuck8" H 6230 7142 50  0000 L CNN
F 1 "Conn_01x03" H 6230 7051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6150 7100 50  0001 C CNN
F 3 "~" H 6150 7100 50  0001 C CNN
	1    6150 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 7000 5600 7000
Text Label 5600 7000 0    50   ~ 0
NTC_CH8
Text Label 5950 7100 2    50   ~ 0
NTC_CH_RET8
Text Label 5950 4350 2    50   ~ 0
NTC+5_ch_4
Text Label 5900 3800 2    50   ~ 0
NTC+5_ch_3
Text Label 5900 3250 2    50   ~ 0
NTC+5_ch_2
Text Label 5900 2650 2    50   ~ 0
NTC+5_ch_1
Text Label 5900 5000 2    50   ~ 0
NTC+5_ch_5
Text Label 5900 5700 2    50   ~ 0
NTC+5_ch_6
Text Label 5950 7200 2    50   ~ 0
NTC+5_ch_8
Text Label 5900 6400 2    50   ~ 0
NTC+5_ch_7
Text Label 5550 2450 2    50   ~ 0
NTC_CH1
Wire Notes Line
	11050 650  15850 650 
Wire Notes Line
	15850 650  15850 2050
Wire Notes Line
	15850 2050 11150 2050
Wire Notes Line
	11150 2050 11150 650 
Wire Notes Line
	15750 3550 15750 2150
Wire Notes Line
	16000 3700 16000 9950
Wire Notes Line
	16000 9950 8450 9950
Wire Notes Line
	8450 9950 8450 3700
Wire Notes Line
	8450 3700 16000 3700
Wire Notes Line
	7250 10150 750  10150
Wire Notes Line
	750  10150 750  8100
Wire Notes Line
	750  8100 7250 8100
Wire Notes Line
	7250 8100 7250 10150
Wire Notes Line
	7450 7500 4800 7500
Wire Notes Line
	550  1600 550  7600
Wire Notes Line
	650  1500 650  550 
Wire Notes Line
	650  550  5000 550 
Wire Notes Line
	5000 550  5000 1500
Wire Notes Line
	650  1500 5000 1500
Text Notes 850  8400 0    168  ~ 0
SHT 85 sensor connectors
Text Notes 650  1950 0    168  ~ 0
Connectors for chuck NTCs
Text Notes 950  750  0    168  ~ 0
Chuck NTC return for 8 chucks
Text Notes 5150 2350 0    168  ~ 0
3 pin individual\nconnectors for \nchucks NTCs
Text Notes 8500 3950 0    168  ~ 0
Power flex connectors from the modules
Text Notes 7600 10400 0    168  ~ 0
Test points
Text Notes 9300 10400 0    89   ~ 0
To power-up modules (option)
Text Notes 4700 8400 0    89   ~ 0
1 external + 1 on the PCB
Text Notes 900  5950 1    128  ~ 0
Jumpers\n(2 options of the connection)
Wire Notes Line
	550  1600 4650 1600
Wire Notes Line
	550  7600 4650 7600
Wire Notes Line
	4650 1600 4650 7600
Text Notes 4450 6100 1    128  ~ 0
2 pin connection of chuck NTC
Text Notes 11400 950  0    168  ~ 0
Module NTC return for 8 modules
Text Notes 8250 3200 0    168  ~ 0
3 pin individual\nconnectors for \nModules NTCs
Wire Notes Line
	8150 2150 15750 2150
Wire Notes Line
	8150 3550 15750 3550
Wire Notes Line
	8150 2150 8150 3600
Wire Notes Line
	4800 1550 4800 7500
Wire Notes Line
	7400 1550 7400 7500
Wire Notes Line
	4800 1550 7450 1550
Wire Notes Line
	7500 500  7500 1650
Wire Notes Line
	7500 1650 10700 1650
Wire Notes Line
	10700 1650 10700 500 
Text Label 1100 7700 2    50   ~ 0
NTC_CH1
Text Label 1750 8000 2    50   ~ 0
NTC_CH8
Text Label 1750 7800 2    50   ~ 0
NTC_CH6
Text Label 1750 7700 2    50   ~ 0
NTC_CH5
Text Label 1100 8000 2    50   ~ 0
NTC_CH4
Text Label 1100 7900 2    50   ~ 0
NTC_CH3
Text Label 1100 7800 2    50   ~ 0
NTC_CH2
Text Label 1750 7900 2    50   ~ 0
NTC_CH7
Text Label 1150 7700 0    50   ~ 0
GND
Text Label 1150 7800 0    50   ~ 0
GND
Text Label 1150 7900 0    50   ~ 0
GND
Text Label 1150 8000 0    50   ~ 0
GND
Text Label 1850 7700 0    50   ~ 0
GND
Text Label 1850 7800 0    50   ~ 0
GND
Text Label 1850 7900 0    50   ~ 0
GND
Text Label 1850 8000 0    50   ~ 0
GND
Text Label 8450 2250 2    50   ~ 0
NTC1
Text Label 8450 2350 2    50   ~ 0
NTC2
Text Label 9100 2250 2    50   ~ 0
NTC3
Text Label 9100 2350 2    50   ~ 0
NTC4
Text Label 9650 2250 2    50   ~ 0
NTC5
Text Label 9650 2350 2    50   ~ 0
NTC6
Text Label 10200 2250 2    50   ~ 0
NTC7
Text Label 10200 2350 2    50   ~ 0
NTC8
Text Label 8550 2250 0    50   ~ 0
GND
Text Label 8550 2350 0    50   ~ 0
GND
Text Label 9200 2350 0    50   ~ 0
GND
Text Label 9200 2250 0    50   ~ 0
GND
Text Label 9750 2250 0    50   ~ 0
GND
Text Label 9750 2350 0    50   ~ 0
GND
Text Label 10300 2250 0    50   ~ 0
GND
Text Label 10300 2350 0    50   ~ 0
GND
Wire Wire Line
	8450 2250 8550 2250
Wire Wire Line
	8450 2350 8550 2350
Wire Wire Line
	9100 2250 9200 2250
Wire Wire Line
	9100 2350 9200 2350
Wire Wire Line
	9650 2250 9750 2250
Wire Wire Line
	9650 2350 9750 2350
Wire Wire Line
	10200 2250 10300 2250
Wire Wire Line
	10200 2350 10300 2350
Wire Wire Line
	1100 7700 1150 7700
Wire Wire Line
	1100 7800 1150 7800
Wire Wire Line
	1100 7900 1150 7900
Wire Wire Line
	1100 8000 1150 8000
Wire Wire Line
	1750 7700 1850 7700
Wire Wire Line
	1750 7800 1850 7800
Wire Wire Line
	1750 7900 1850 7900
Wire Wire Line
	1750 8000 1850 8000
Wire Notes Line
	10700 500  7500 500 
Text Notes 8850 1600 0    50   ~ 0
1017503 
Text Notes 7600 1000 0    168  ~ 0
Common VDD and GND\nConnector
Text Notes 13400 2000 0    50   ~ 0
\n1017505\n
Text Notes 14850 2000 0    50   ~ 0
\n1017505\n
Text Notes 2650 1350 0    42   ~ 0
Phoenix Contact \n1751189 
Text Notes 1450 2350 0    50   ~ 0
691347000002B 
Text Notes 1300 9850 0    50   ~ 0
\n691347000004B\n
$EndSCHEMATC
